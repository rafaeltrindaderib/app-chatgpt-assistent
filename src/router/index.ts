// src/router/index.ts
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import HomeComponent from '../views/HomeView.vue';
import PanelView from '../views/PanelAdmView.vue';
import ConversationViewVue from '../views/ConversationView.vue';



const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: HomeComponent,
  },
  {
    path: '/conversation',
    name: 'conversation',
    component: ConversationViewVue,
  },
  {
    path: '/panel',
    name: 'panel',
    component: PanelView,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
