// src/plugins/vuetify.ts
import { createVuetify } from 'vuetify'
import 'vuetify/styles' // Importa os estilos do Vuetify

export default createVuetify() // Cria e exporta a instância do Vuetify
